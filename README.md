Form2Clip
=========

This application serves a web-form whose data are copied in the clipboard.
It is designed to work in Linux, with a Wayland GUI.

The motivation is to be able to use a smart-phone proprietary speech-to-text feature
to type things on my laptop.

Requirements
------------

* all Python packages listed in `requirements.txt`; install with
  ```
  pip install -r requirements.txt
  ```
* command line tools `wl-copy` and `notify-send`; in Ubuntu:
  ```
  sudo apt install wl-clipboard libnotify-bin
  ```
