#!/usr/bin/env python
from flask import Flask, request
import subprocess

app = Flask(__name__)

@app.route("/", methods=['GET', 'POST'])
def root():
    text = request.form.get('text')
    if text:
        subprocess.run(['wl-copy', text])
        subprocess.run(['notify-send', '-e', 'copied', text])
    return FORM

FORM = '''<!DOCTYPE html>
<title>Form2Clip</title>
<meta name="viewport" content="width=device-width">
<style>
    body { margin 0.2em 0.1em; }
    textarea { width: 100%; height: 60vh; }
    button { width: 100%; }
</style>
<form method=post action="">
<button>Send</button>
<textarea name=text placeholder="type or dictate">
</textarea>
<button>Send</button>
</form>
'''

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
